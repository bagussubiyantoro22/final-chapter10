import { expect, jest, test, it, describe } from "@jest/globals";
global.jest = jest;

describe("arrayContaining", () => {
  const expected = ["link"];
  it("matches even if received contains additional elements", () => {
    expect(["link"]).toEqual(expect.arrayContaining(expected));
  });
  it("does not match if received does not contain expected elements", () => {
    expect(["brokenlink"]).not.toEqual(expect.arrayContaining(expected));
  });
});

describe("stringMatching in arrayContaining", () => {
  const expected = [
    expect.stringMatching(/^button/),
    expect.stringMatching(/^RegisterComp/),
  ];
  it("matches even if received contains additional elements", () => {
    expect(["button", "RegisterComp", "menu"]).toEqual(
      expect.arrayContaining(expected)
    );
  });
  it("does not match if received does not contain expected elements", () => {
    expect(["button", "menu"]).not.toEqual(expect.arrayContaining(expected));
  });
});

describe("not.stringContaining", () => {
  const expected = "RegisterComp";

  it("matches if the received value does not contain the expected substring", () => {
    expect("linkdown").toEqual(expect.not.stringContaining(expected));
  });
});
