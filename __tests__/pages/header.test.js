import { expect, jest, test, it, describe } from "@jest/globals";
global.jest = jest;
// import { Header } from "@/pages/Header";

// const Header = require("@/pages/Header/index");

describe("arrayContaining", () => {
  const expected = ["link"];
  it("matches even if received contains additional elements", () => {
    expect(["link"]).toEqual(expect.arrayContaining(expected));
  });
  it("does not match if received does not contain expected elements", () => {
    expect(["brokenlink"]).not.toEqual(expect.arrayContaining(expected));
  });
});

describe("stringMatching in arrayContaining", () => {
  const expected = [
    expect.stringMatching(/^button/),
    expect.stringMatching(/^Header/),
  ];
  it("matches even if received contains additional elements", () => {
    expect(["button", "Header", "menu"]).toEqual(
      expect.arrayContaining(expected)
    );
  });
  it("does not match if received does not contain expected elements", () => {
    expect(["button", "menu"]).not.toEqual(expect.arrayContaining(expected));
  });
});

// test("resolves to gamelist", async () => {
//   await expect(Promise.resolve("Header")).resolves.toBe("gamelist");
//   await expect(Promise.resolve("gamelist")).resolves.not.toBe("about");
// });

describe("not.stringContaining", () => {
  const expected = "Header";

  it("matches if the received value does not contain the expected substring", () => {
    expect("linkdown").toEqual(expect.not.stringContaining(expected));
  });
});
