import Header from "../Header";
import React from "react";
import { Player } from "video-react";

export default function Home() {
  return (
    <>
      <Header />

      <h1>VIDEO GALLERY</h1>
      <Player playsInline fluid={false} width={1080} height={520}>
        <source src="https://res.cloudinary.com/dpn87by16/video/upload/v1677770769/AOT_Hardstyle_My_Soldiers_Rage_-_Hiroyuki_Sawano_Maul_Bootleg_nsbpnt.mp4" />
      </Player>
    </>
  );
}
