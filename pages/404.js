import { Text } from "@nextui-org/react";
import { useRouter } from "next/router";
import { useEffect } from "react";

export default function Page404() {
  const router = useRouter();
  useEffect(() => {
    setTimeout(() => {
      router.push("/");
    }, 2000);
  },);

  return (
    <div>
      <Text
        h1
        size={40}
        color="warning"
        css={{
          textAlign: "center",
        }}
        weight="bold"
      >
        Oooppss ...
      </Text>
      <Text
        h2
        size={30}
        color="warning"
        css={{
          textAlign: "center",
        }}
        weight="bold"
      >
        Halaman yang anda cari tidak ditemukan
      </Text>
    </div>
  );
}
