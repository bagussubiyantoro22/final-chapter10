import Box from "../../Box";
import { Text, Grid, Card, Row, Input, Button, Loading } from "@nextui-org/react";
import React from "react";

export default function ContentProfile() {
  return (
    <div>
      <Box
        css={{
          textAlign: "center",
          px: "$12",
          mt: "$10",
          "@xsMax": { px: "$10" },
        }}
      >
        <Grid.Container justify="center">
          <Grid sm={12} md={5}>
            <Card variant="bordered">
              <Card.Header>
                <Text b>PROFILE</Text>
              </Card.Header>
              <Card.Divider />
              <Card.Body>
                <Input label="email" placeholder="email" />
                <Input label="Fullname" placeholder="fullname" />
                <Input label="Alamat" placeholder="Alamat" />
                {/* <Input label="Zodiak" placeholder="leo" /> */}
              </Card.Body>

              <Card.Footer>
                <Row justify="center">
                  <Button size="sm" labelPlaceholder="Loading..." contentRight={<Loading size="xs" />}>
                    SAVE
                  </Button>
                </Row>
              </Card.Footer>
            </Card>
          </Grid>
        </Grid.Container>
      </Box>
    </div>
  );
}
