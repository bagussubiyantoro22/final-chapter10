import { NextUIProvider } from "@nextui-org/react";
import { Provider } from "react-redux";
import store from "../redux/store";
import "../styles/rps.css";
import "../styles/leaderboard.css";
import "../styles/dummyGames.css";
import "../styles/module.media.css";
import { AuthProvider } from "@/Context/auth";

function MyApp({ Component, pageProps }) {
  return (
    <NextUIProvider>
      <AuthProvider>
        <Provider store={store}>
          <Component {...pageProps} />
        </Provider>
      </AuthProvider>
    </NextUIProvider>
  );
}

export default MyApp;
