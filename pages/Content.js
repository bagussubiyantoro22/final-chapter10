import { Text } from "@nextui-org/react";
import Box from "./Box";

const Content = () => (
  <Box
    css={{ textAlign: "center", px: "$12", mt: "$8", "@xsMax": { px: "$10" } }}
  >
    <Text
      h2
      size={50}
      css={{
        textGradient: "15deg, #2563EB 30%, #4A00E0 50%",
      }}
      weight="bold"
    >
      Game kelompok Satu Binar
    </Text>
    <Text size="$xl">
      website game yang menyediakan pengalaman bermain yang seru dan
      menyenangkan.
    </Text>
  </Box>
);

export default Content
