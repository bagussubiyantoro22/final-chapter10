import { Container, Navbar, Text } from "@nextui-org/react";

export default function Footer() {
  return (
    <Container>
      <Navbar
        css={{
          justifyContent: "fixed-bottom",
          // variant ="floating"
        }}
      >
        <Navbar.Brand
          css={{
            justifyContent: "fixed-bottom",
            // variant ="floating"
          }}
        >
          <Text b color="inherit" hideIn="xs">
            Kelompok Satu Binar | Wave 26 | Bege, Pandu, Aziz, Fauzan,
            Herlambang
          </Text>
        </Navbar.Brand>
      </Navbar>
    </Container>
  );
}
