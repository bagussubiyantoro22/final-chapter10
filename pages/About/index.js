import Header from "../Header";
import ContentAbout from "./ContentAbout/ContentAbout.js";

export default function About() {
  return (
    <>
      <Header />
      <ContentAbout />
      <title>Binar | Kelompok-2</title>
    </>
  );
}
