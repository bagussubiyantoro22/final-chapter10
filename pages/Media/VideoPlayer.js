import { Input, Button, Spacer } from "@nextui-org/react";
import React, { useState } from "react";
import ReactPlayer from "react-player";

const VideoPlayer = ({}) => {
  const [showPlayer, setShowPlayer] = useState(false);
  const [videoUrl, setVideoUrl] = useState();

  const handleClick = () => {
    setShowPlayer(!showPlayer);
  };

  return (
    <div>
      <Input
        placeholder="Enter video link"
        onChange={(e) => setVideoUrl(e.target.value)}
      />
      <Spacer y={0.8} />
      <Button onClick={handleClick}>Play Video</Button>
      {showPlayer && (
        <ReactPlayer
          url={videoUrl}
          controls={true}
          width="50%"
          height="500px"
        />
      )}
    </div>
  );
};

export default VideoPlayer;
