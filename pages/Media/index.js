import React from "react";
import Header from "../Header";
import VideoPlayer from "./VideoPlayer.js";

const MyPage = () => (
  <div>
    <Header />
    <VideoPlayer videoUrl="" />
  </div>
);

export default MyPage;
