import { configureStore } from "@reduxjs/toolkit";
import counterSlice from "../reducer";
import gameReducer from "../../src/slices/gameSlice";

export const store = configureStore({
  reducer: {
    game: gameReducer,
    counterReducer: counterSlice.reducer,
  },
});

export default store;
