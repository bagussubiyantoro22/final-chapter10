import { calculateWinner, computerChoice } from "@/pages/RpsGame";
import { createSlice } from "@reduxjs/toolkit";

const gameSlice = createSlice({
  name: "game",
  initialState: {
    playerChoice: "",
    computerChoice: "",
    result: "",
    playerScore: 0,
    computerScore: 0,
  },
  reducers: {
    choose: (state, action) => {
      state.playerChoice = action.payload;
      state.computerChoice = computerChoice();
      state.result = calculateWinner(state.playerChoice, state.computerChoice);
      if (state.result === -1) state.playerScore++;
      if (state.result === 1) state.computerScore++;
    },
  },
});

export const { choose } = gameSlice.actions;
export default gameSlice.reducer;
